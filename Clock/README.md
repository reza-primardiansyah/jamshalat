= INSTALLATION
use at least python 3.7
install pip

install dependencies by running
```
pip install -r requirements.txt

```

= RUNNING
execute this command.

```
python PiClockClassic.py
```
