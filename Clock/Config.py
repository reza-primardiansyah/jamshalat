from GoogleMercatorProjection import LatLng
from PySide2.QtGui import QColor

#background yg di gunakan di MainSlideFrame tema classic
mainclassicbg = 'images/green.png'

#background yg di gunakan di frame adzan dan shalat di semua tema 
adzanBackground = 'images/black.jpg'
shalatBackground = 'images/black.jpg'

#sound adzan di tema classic dan non-classic
adzans ='sounds/adzan_tes.wav'
adzan_fajr = 'sounds/adzan-fajr.mp3'

notify_fajr = "sounds/sholat-subuh.mp3"
notify_zhuhur = "sounds/sholat-zhuhur.mp3"
notify_ashar = "sounds/sholat-ashar.mp3"
notify_maghrib = "sounds/sholat-maghrib.mp3"
notify_isya = "sounds/sholat-isya.mp3"

#logo perusahaan di pakai di semua tema
logo = 'images/sabranglor.png'

#logo produk di semua tema
product = 'images/pasowan.png'
product2 = 'images/circle-cropped.png'

# warna default yg di pakai tema classic
textcolor2 = 'white'

# Text saat frame adzan tema classic dan non-classic
TextAdzan = 'Sedang adzan harap tenang dan matikan/silent handphone anda'

# Text saat adzan frame
doPray = 'Sedang Shalat'

# Text hadist hari ini
hadistText = 'Hadits hari ini'

# Text waktu iqamah
iqamahTime = 'Menuju Waktu Iqamah'

# Text waktu shalat
TimeShalat = 'Waktu Shalat'

# Text frame info
info = 'Info untuk Anda'
infoText = 'Text Pengumuman di Masjid'

# Text yg di gunakan di main slide frame classic
mainInfo = 'Mohon matikan atau silent Handphone anda demi ketenangan dan kekhusyukan ibadah shalat kita'

# Text di frame support 
supportText = 'Jam Shalat Pasowan\nPenjualan & Dukungan'
supportAdress = 'WA : 0812345678\nsales@pasowan.id\nsupport@pasowan.id\nwww.pasowan.id'

# format waktu digital yg di gunakan di tema classic dan non-classic
digitalformat = "{0:%H:%M}"

# Warna yang digunakan di tema classic dan warna text di non classic
digitalcolor3 = "#CBEB50"

# Warna default countdown tarhim di iqamah frame classic dan non-classic theme
digitalColorCountdownTarhim ="#FFFFFF"

# ukuran font default countdown tarhim di iqamah frame classic dan non-classic
digitalsizeCountdownIqamah = 75

#data dummy untuk info masjid dan web di main frame non classic theme
welcome = "Ahlan Wa Sahlan "
name = "Masjid Bening Guru Semesta"
address = "Jl Ampera Raya - Jakarta"
phone = "021-12345668"
website = 'www.bgs.co.id'

#background main frame tema non-classic
background9 = 'images/background.jpeg'

#ukuran jam digital pada main frame tema non-classic
mainframeClockDigitSize = 50

#background tarhim frame tema non-classic
tarhimbg = 'images/bg30.jpg'

#glow color tarhim frame non classic
digitalcolor5 = "#a4e075"

#color text tarhim frame non classic
textcolor = '#bef'

# picture yg di insert di tarhim frame
attention = 'images/attention.png'
slash = 'images/label.png'

#background adzan frame non-classic theme
adzanbg = 'images/bg10'

#font size info adzan dan image di frame adzan non-classic
digitalsize4 = 25
soundless = 'images/silent.jpeg'

#background iqamah frame non-classic theme
newiqamah = 'images/bg13.jpg'

#config jam analog di shalat frame
clockface = 'images/ClockfaceNew.png'
hourhand = 'images/hourhand.png'
minhand = 'images/minhand.png'
sechand = 'images/sechand.png'

# LOCATION(S)
# Further radar configuration (zoom, marker location) can be
# completed under the RADAR section
#primary_coordinates = 44.9764016, -93.2486732  # Change to your Lat/Lon
#primary_coordinates = -6.385589, 106.830711 # Depok, West Java, Indonesia
primary_coordinates = -6.175110, 106.865036 #Jakarta
lat = -6.175110
lon = 106.865036
timezone = +7

location = LatLng(primary_coordinates[0], primary_coordinates[1])
primary_location = LatLng(primary_coordinates[0], primary_coordinates[1])

digital = 1             # 1 = Digtal Clock, 0 = Analog Clock

# The above example shows in this way:
#  https://github.com/n0bel/PiClock/blob/master/Documentation/Digital%20Clock%20v1.jpg
# ( specifications of the time string are documented here:
#  https://docs.python.org/2/library/time.html#time.strftime )

# digitalformat = "{0:%I:%M}"
# digitalsize = 250
#  The above example shows in this way:
#  https://github.com/n0bel/PiClock/blob/master/Documentation/Digital%20Clock%20v2.jpg

usemapbox = 1   # Use Mapbox.com for maps, needs api key (mbapi in ApiKeys.py)
metric = 0  # 0 = English, 1 = Metric
radar_refresh = 10      # minutes
weather_refresh = 30    # minutes
# Wind in degrees instead of cardinal 0 = cardinal, 1 = degrees
wind_degrees = 0

# gives all text additional attributes using QT style notation
# example: fontattr = 'font-weight: bold; '
fontattr = ''

# These are to dim the radar images, if needed.
# see and try Config-Example-Bedside.py
dimcolor = QColor('#000000')
dimcolor.setAlpha(0)

# Language Specific wording
# DarkSky Language code
#  (https://darksky.net/dev/docs under lang=)
Language = "ID"

# The Python Locale for date/time (locale.setlocale)
#  '' for default Pi Setting
# Locales must be installed in your Pi.. to check what is installed
# locale -a
# to install locales
# sudo dpkg-reconfigure locales
DateLocale = 'id_ID.utf-8'
DateLocale2 = 'en_GB.utf-8'

# Language specific wording
LPressure = "Pressure "
LHumidity = "Humidity "
LWind = "Wind "
Lgusting = " gusting "
LFeelslike = "Feels like "
LPrecip1hr = " Precip 1hr:"
LToday = "Today: "
LSunRise = "Sun Rise:"
LSet = " Set: "
LMoonPhase = " Moon Phase:"
LInsideTemp = "Inside Temp "
LRain = " Rain: "
LSnow = " Snow: "
Lmoon1 = 'New Moon'
Lmoon2 = 'Waxing Crescent'
Lmoon3 = 'First Quarter'
Lmoon4 = 'Waxing Gibbous'
Lmoon5 = 'Full Moon'
Lmoon6 = 'Waning Gibbous'
Lmoon7 = 'Third Quarter'
Lmoon8 = 'Waning Crecent'

# RADAR
# By default, primary_location entered will be the
#  center and marker of all radar images.
# To update centers/markers, change radar sections below the desired lat/lon as:
# -FROM-
# primary_location,
# -TO-
# LatLng(44.9764016,-93.2486732),
radar1 = {
    'center': primary_location,  # the center of your radar block
    'zoom': 7,  # this is a maps zoom factor, bigger = smaller area
    'style': 'mapbox/satellite-streets-v10',  # optional style (mapbox only)
    'markers': (   # google maps markers can be overlayed
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
            'image': 'teardrop-dot',  # optional image from the markers folder
        },          # dangling comma is on purpose.
    )
}


radar2 = {
    'center': primary_location,
    'zoom': 11,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}


radar3 = {
    'center': primary_location,
    'zoom': 7,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}

radar4 = {
    'center': primary_location,
    'zoom': 11,
    'markers': (
        {
            'location': primary_location,
            'color': 'red',
            'size': 'small',
        },
    )
}
