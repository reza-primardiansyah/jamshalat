from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class SupportFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("supportFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#supportFrame { background-color: transparent; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockSupport = QtWidgets.QLabel(self)
        self.clockSupport.setObjectName("clockSupport")
        self.clockSupport.setGeometry(50 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        self.clockSupport.setStyleSheet(
           "#clockSupport { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: "+config.textcolor2+"; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockSupport.setAlignment(Qt.AlignLeft)

        self.todayDaySupport = QtWidgets.QLabel(self)
        self.todayDaySupport.setObjectName("todayDaySupport")
        self.todayDaySupport.setStyleSheet("#todayDaySupport { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDaySupport.setAlignment(Qt.AlignLeft)
        self.todayDaySupport.setGeometry(50 * xscale, 125 * yscale, 500 * xscale, 100)

        self.todayDateSupport = QtWidgets.QLabel(self)
        self.todayDateSupport.setObjectName("todayDateSupport")
        self.todayDateSupport.setStyleSheet("#todayDateSupport { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDateSupport.setAlignment(Qt.AlignLeft)
        self.todayDateSupport.setGeometry(50 * xscale, 175 * yscale, 500 * xscale, 100)

        self.hijriDateSupport = QtWidgets.QLabel(self)
        self.hijriDateSupport.setObjectName("hijriDateSupport")
        self.hijriDateSupport.setStyleSheet("#hijriDateSupport { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.hijriDateSupport.setAlignment(Qt.AlignLeft)
        self.hijriDateSupport.setGeometry(50 * xscale, 225 * yscale, 500 * xscale, 100)

        self.insertPictSupport = QtWidgets.QLabel(self)
        self.insertPictSupport.setObjectName("insertPictSupport")
        self.insertPictSupport.setStyleSheet("#insertPictSupport {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictSupport.resize(width_label, height_label)
        self.insertPictSupport.setPixmap(pixmap.scaled(self.insertPictSupport.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictSupport.setGeometry(635 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

        self.labelSupport = QtWidgets.QLabel(self)
        self.labelSupport.setObjectName("labelSupport")
        self.labelSupport.setWordWrap(True) 
        self.labelSupport.setStyleSheet("#labelSupport { font-family:Trebuchet MS; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(40 * xscale)) + "px;}")
        self.labelSupport.setAlignment(Qt.AlignHCenter)
        self.labelSupport.setGeometry(125 * xscale, 450 * yscale, 500 * xscale, 500 * yscale)
        self.labelSupport.setText(config.supportText)

        self.supportAdress = QtWidgets.QLabel(self)
        self.supportAdress.setObjectName("supportAdress")
        self.supportAdress.setWordWrap(True)
        self.supportAdress.setStyleSheet("#supportAdress { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(40 * xscale)) + "px;}")
        self.supportAdress.setAlignment(Qt.AlignLeft)
        self.supportAdress.setGeometry(900 * xscale, 375 * yscale, 600 * xscale, 1500 * yscale)
        self.supportAdress.setText(config.supportAdress)

    def updateClock(self, timestr):
        self.clockSupport.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDaySupport.setText(day)
        self.todayDateSupport.setText(ds)
        self.hijriDateSupport.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))

