from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class InfoFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("infoFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#infoFrame { background-color: transparent; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockInfo = QtWidgets.QLabel(self)
        self.clockInfo.setObjectName("clockInfo")
        self.clockInfo.setGeometry(50 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        self.clockInfo.setStyleSheet(
           "#clockInfo { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: "+config.textcolor2+"; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockInfo.setAlignment(Qt.AlignLeft)

        self.todayDayInfo = QtWidgets.QLabel(self)
        self.todayDayInfo.setObjectName("todayDayInfo")
        self.todayDayInfo.setStyleSheet("#todayDayInfo { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDayInfo.setAlignment(Qt.AlignLeft)
        self.todayDayInfo.setGeometry(50 * xscale, 125 * yscale, 500 * xscale, 100)

        self.todayDateInfo = QtWidgets.QLabel(self)
        self.todayDateInfo.setObjectName("todayDateInfo")
        self.todayDateInfo.setStyleSheet("#todayDateInfo { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDateInfo.setAlignment(Qt.AlignLeft)
        self.todayDateInfo.setGeometry(50 * xscale, 175 * yscale, 500 * xscale, 100)

        self.hijriDateInfo = QtWidgets.QLabel(self)
        self.hijriDateInfo.setObjectName("hijriDateInfo")
        self.hijriDateInfo.setStyleSheet("#hijriDateInfo { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.hijriDateInfo.setAlignment(Qt.AlignLeft)
        self.hijriDateInfo.setGeometry(50 * xscale, 225 * yscale, 500 * xscale, 100)

        self.info = QtWidgets.QLabel(self)
        self.info.setObjectName("info")
        self.info.setWordWrap(True) 
        self.info.setStyleSheet("#info { font-family:Trebuchet MS; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(50 * xscale)) + "px;}")
        self.info.setAlignment(Qt.AlignJustify)
        self.info.setGeometry(175 * xscale, 450 * yscale, 400 * xscale, 100 * yscale)
        self.info.setText(config.info)

        self.infoText = QtWidgets.QLabel(self)
        self.infoText.setObjectName("infoText")
        self.infoText.setWordWrap(True)
        self.infoText.setStyleSheet("#infoText { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.infoText.setAlignment(Qt.AlignLeft)
        self.infoText.setGeometry(800 * xscale, 375 * yscale, 600 * xscale, 1500 * yscale)
        self.infoText.setText(config.infoText)

        self.insertPictInfo = QtWidgets.QLabel(self)
        self.insertPictInfo.setObjectName("insertPictInfo")
        self.insertPictInfo.setStyleSheet("#insertPictInfo {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictInfo.resize(width_label, height_label)
        self.insertPictInfo.setPixmap(pixmap.scaled(self.insertPictInfo.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictInfo.setGeometry(635 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

    def updateClock(self, timestr):
        self.clockInfo.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDayInfo.setText(day)
        self.todayDateInfo.setText(ds)
        self.hijriDateInfo.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))

    def updateInfoDisplay(self,info):
        self.infoText.setText(info['fields']['konten'])

    def showDefaultInfoDisplay(self):
        self.infoText.setText("Tidak ada pengumuman")

