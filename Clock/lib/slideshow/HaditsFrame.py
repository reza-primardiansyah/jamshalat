from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtCore import Qt

islamicMonths = {
1:'Muharram', 2:'Shafar', 3:'Rabiul Awwal', 4:'Rabiul Akhir', 5:'Jumadil Awwal', 6:'Jumadil Akhir',
7:'Rajab', 8:'Sya\'ban', 9:'Ramadhan', 10:'Syawwal', 11:'Dzulqa\'idah', 12:'Dzulhijjah'
}

class HaditsFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):
        QtWidgets.QFrame.__init__(self, parent)
        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0
        self.setObjectName("haditsFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#haditsFrame { background-color: black; border-image: url(" +
                     config.mainclassicbg + ") 0 0 0 0 stretch stretch;}")
        self.clockIqamah = QtWidgets.QLabel(self)
        self.clockIqamah.setObjectName("clockIqamah")
        self.clockIqamah.setGeometry(50 * xscale, 25 * yscale, 370 * xscale, 100 * yscale)
        self.clockIqamah.setStyleSheet(
           "#clockIqamah { background-color: transparent; font-family:Trebuchet MS;" +
           " font-weight: bold; color: "+config.textcolor2+"; font-size: " +
           str(int(80 * xscale)) +
           "px;}")
        self.clockIqamah.setAlignment(Qt.AlignLeft)

        self.todayDayHadist = QtWidgets.QLabel(self)
        self.todayDayHadist.setObjectName("todayDayHadist")
        self.todayDayHadist.setStyleSheet("#todayDayHadist { font-family:Trebuchet MS; font-weight:light; color: " +
                            "white " +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDayHadist.setAlignment(Qt.AlignLeft)
        self.todayDayHadist.setGeometry(50 * xscale, 125 * yscale, 500 * xscale, 100)

        self.todayDateHadist = QtWidgets.QLabel(self)
        self.todayDateHadist.setObjectName("todayDateHadist")
        self.todayDateHadist.setStyleSheet("#todayDateHadist { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.todayDateHadist.setAlignment(Qt.AlignLeft)
        self.todayDateHadist.setGeometry(50 * xscale, 175 * yscale, 500 * xscale, 100)

        self.hijriDateHadist = QtWidgets.QLabel(self)
        self.hijriDateHadist.setObjectName("hijriDateHadist")
        self.hijriDateHadist.setStyleSheet("#hijriDateHadist { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                            "; background-color: transparent; font-size: " +
                            str(int(40 * xscale)) +
                            "px;}")
        self.hijriDateHadist.setAlignment(Qt.AlignLeft)
        self.hijriDateHadist.setGeometry(50 * xscale, 225 * yscale, 500 * xscale, 100)


        self.haditsText = QtWidgets.QLabel(self)
        self.haditsText.setObjectName("haditsText")
        self.haditsText.setWordWrap(True)
        self.haditsText.setStyleSheet("#haditsText { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(50 * xscale)) + "px;}")
        self.haditsText.setAlignment(Qt.AlignLeft)
        self.haditsText.setGeometry(175 * xscale, 450 * yscale, 700 * xscale, 1000 * yscale)
        self.haditsText.setText(config.hadistText)

        self.insertPictIqamah = QtWidgets.QLabel(self)
        self.insertPictIqamah.setObjectName("insertPictIqamah")
        self.insertPictIqamah.setStyleSheet("#insertPictIqamah {background-color: transparent;}")
        pixmap = QtGui.QPixmap(config.logo)
        height_label = 130
        width_label = 130
        self.insertPictIqamah.resize(width_label, height_label)
        self.insertPictIqamah.setPixmap(pixmap.scaled(self.insertPictIqamah.size(), QtCore.Qt.KeepAspectRatio))
        self.insertPictIqamah.setGeometry(635 * xscale, 775 * yscale, 150 * xscale, 100 * yscale)

        self.contentHadits = QtWidgets.QLabel(self)
        self.contentHadits.setObjectName("contentHadits")
        self.contentHadits.setWordWrap(True)
        self.contentHadits.setStyleSheet("#contentHadits { font-family:Trebuchet MS; font-weight:light; color: " +
                            config.textcolor2 +
                             "; background-color: transparent; font-size: " +
                             str(int(30 * xscale)) + "px;}")
        self.contentHadits.setAlignment(Qt.AlignLeft)
        self.contentHadits.setGeometry(800 * xscale, 320 * yscale, 600 * xscale, 1500 * yscale)


    def updateClock(self, timestr):
        self.clockIqamah.setText(timestr.lower())

    def updateDate(self, day, ds, islamicDate):
        self.todayDayHadist.setText(day)
        self.todayDateHadist.setText(ds)
        self.hijriDateHadist.setText("{0} {1} {2}".format(islamicDate[2], islamicMonths[islamicDate[1]], islamicDate[0]))

    def updateHaditsDisplay(self,hadits):
        self.contentHadits.setText(hadits['fields']['konten'])
