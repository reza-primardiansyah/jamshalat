import datetime

class WaktuShalat:
	with_adzan = {
		"imsak":False,
		"fajr":True,
		"sunrise":False,
		"dhuhr":True,
		"asr":True,
		"sunset":False,
		"maghrib":True,
		"isha":True,
		"midnight":False
	}
	def parseWaktu(self, waktu):
		hour, minute = [int(i) for i in waktu.split(':')]
		return datetime.time(hour=hour, minute=minute)
	def parseDurasi(self, waktu):
		minutes, seconds = [int(i) for i in waktu.split(':')]
		return datetime.timedelta(minutes=minutes, seconds=seconds)

	def __init__(self, dict, selesai_dict):
		self.nama = dict["nama"]
		self.waktu = self.parseWaktu(dict["waktu"])
		self.selesai = self.parseWaktu(selesai_dict["waktu"])
		self.awal = self.hitung_awal()
		self.akhir = self.hitung_akhir()
		self.durasi_adzan = self.parseDurasi(dict["durasi_adzan"])
		self.durasi_iqomah = self.parseDurasi(dict["durasi_iqomah"])
		self.durasi_tarhim = self.parseDurasi(dict["durasi_tarhim"])
		self.durasi_sholat = self.parseDurasi(dict["durasi_sholat"])
		self.has_adzan =  self.with_adzan[dict["nama"]] if dict["nama"] in self.with_adzan else False

	def __str__(self):
		return "{} {}-{} {}".format(self.nama, self.awal, self.akhir, self.has_adzan)
	def hitung_akhir(self):
		now = datetime.datetime.now()
		if(self.is_syuruq()):
			akhir = now.replace(hour=self.waktu.hour,minute=self.waktu.minute,second=0,microsecond=0) + datetime.timedelta(minutes=1)
		elif(self.is_isya()):
			akhir = now.replace(hour=self.selesai.hour,minute=self.selesai.minute,second=0,microsecond=0) + datetime.timedelta(days=1)
		else:
			akhir = now.replace(hour=self.selesai.hour,minute=self.selesai.minute,second=0,microsecond=0)
		return akhir
	def hitung_awal(self):
		now = datetime.datetime.now()
		awal = now.replace(hour=self.waktu.hour,minute=self.waktu.minute,second=0,microsecond=0)
		return awal

	def is_before_end(self, now):
		return self.akhir > now
	def is_not_before_start(self, now):
		return self.awal <= now
	def is_after(self, now):
		return self.awal > now

	def is_syuruq(self):
		return self.nama == 'sunrise'
	def is_isya(self):
		return self.nama == 'isha'
	def is_in(self, now):
		return self.is_not_before_start(now) and self.is_before_end(now)

class JadwalSholat:
	def __init__(self, list):
		self.jadwal = self.convertJadwalSholat(list)
	def convertJadwalSholat(self, jadwal):
		used = {
			"imsak":False,
			"fajr":True,
			"sunrise":True,
			"dhuhr":True,
			"asr":True,
			"sunset":False,
			"maghrib":True,
			"isha":True,
			"midnight":False
		}
		used_jadwal = [a for a in jadwal if used[a['nama']]]
		rotated_jadwal = used_jadwal[1:]+used_jadwal[:1]
		times = [WaktuShalat(a,b) for a,b in zip(used_jadwal, rotated_jadwal)]
		result = {}
		for x in times:
			result[x.nama]=x
		return result

	def keys(self):
		return self.jadwal.keys()
	def items(self):
		return self.jadwal.items()
	def as_list(self):
		return [self.jadwal[i.lower()] for i in ['Fajr', 'Sunrise', 'Dhuhr', 'Asr', 'Maghrib', 'Isha']]
