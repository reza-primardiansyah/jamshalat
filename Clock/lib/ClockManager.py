import datetime

class ClockManager:

    # ADZAN_TO_IQAMAH_DURATION = 10 * 60
    # IQAMAH_TO_SHALAT_DURATION = 15
    # SHALAT_DURATION = 10 * 60
    ADZAN_DURATION = 1 * 60

    seconds_hms = [3600, 60, 1] # Number of seconds in an Hour, Minute, and Second

    def isTimeForTarhim(self, now, pray_time):
        time_diff = self.time_diff_seconds(now, pray_time)
        return time_diff > 0 and time_diff <= 60

    def isTimeForAdzan(self, now, pray_time):
        time_diff = self.time_diff_seconds(now, pray_time) 
        return time_diff <= 0 and time_diff >= -10

    def isTimeToShowNormalBeforeIqamah(self, now, pray_time):
        return self.current_time_seconds(now) >= self.alarm_seconds(pray_time) + self.ADZAN_DURATION

    def isTimeForIqamah(self, now, pray_time):
        result = self.time_adzan_seconds(now, pray_time)
        return result  <= 0 and result >= -15

    def isTimeForShalat(self, now, pray_time):
        result = self.time_iqamah_seconds(now, pray_time) 
        return result <= 0 and result >= -15

    def isShalatDone(self, now, pray_time):
        print("shalat_seconds = %d"%self.time_shalat_seconds(now, pray_time))
        result = self.time_shalat_seconds(now, pray_time)
        return result  <= 0 and result >= -15

    def alarm_seconds(self, pray_time):
        hour = pray_time.waktu.hour
        minute = pray_time.waktu.minute
        alarm_time = [hour, minute]

        return sum([a*b for a,b in zip(self.seconds_hms[:len(alarm_time)], alarm_time)])

    def time_diff_seconds(self, now, pray_time):
        result = self.alarm_seconds(pray_time) - self.current_time_seconds(now)
        if(result<-10):
            result += 86400
        return result

    def next_time(self, jadwal, now):
        index = self.next_time_index(jadwal, now)
        if index == -1:
            return None
        return jadwal[index]

    def time_diff_next(self, jadwal, now):
        pray_time = self.next_time(jadwal, now)
        if pray_time == None:
            return 0
        return self.time_diff_seconds(now, pray_time)

    def next_time_index(self, jadwal, now):
        try:
            return min(idx for idx, val in enumerate(jadwal) if val.is_after(now))
        except:
            return 0

    def current_time_index(self, jadwal, now):
        try:
            is_ins = [a.is_in(now) for a in jadwal]
            a_idx = is_ins.index(True)
            return a_idx
        except:
            return -1


    def current_time_seconds(self, now):
        return sum([a*b for a,b in zip(self.seconds_hms, [now.hour, now.minute, now.second])])

    def duration_seconds(self, duration):
        temp = datetime.datetime.now().replace(hour=0,minute=0,second=0,microsecond=0)+duration
        minute = temp.minute
        second = temp.second
        alarm_time = [0, minute, second]
        return sum([a*b for a,b in zip(self.seconds_hms[:len(alarm_time)], alarm_time)])

    def time_for_iqamah(self, pray_time):
        # return self.alarm_seconds(pray_time)+self.ADZAN_TO_IQAMAH_DURATION
        return self.alarm_seconds(pray_time)+self.duration_seconds(pray_time.durasi_adzan)

    def time_for_shalat(self, pray_time):
        # return self.time_for_iqamah(pray_time)+self.IQAMAH_TO_SHALAT_DURATION
        return self.time_for_iqamah(pray_time)+self.duration_seconds(pray_time.durasi_iqomah)

    def time_shalat_ends(self, pray_time):
        # return self.time_for_shalat(pray_time)+self.SHALAT_DURATION
        return self.time_for_shalat(pray_time)+self.duration_seconds(pray_time.durasi_sholat)

    def time_adzan_seconds(self, now, pray_time):
        return self.time_for_iqamah(pray_time) - self.current_time_seconds(now)

    def time_iqamah_seconds(self, now, pray_time):
        return self.time_for_shalat(pray_time) - self.current_time_seconds(now)

    def time_shalat_seconds(self, now, pray_time):
        return self.time_shalat_ends(pray_time) - self.current_time_seconds(now)
