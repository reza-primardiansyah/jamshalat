from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork

class IqamahFrame(QtWidgets.QFrame):
    def __init__(self, parent, config, width, height):

        QtWidgets.QFrame.__init__(self, parent)
        self.setObjectName("iqamahFrame")
        self.setGeometry(0, 0, width, height)
        self.setStyleSheet("#iqamahFrame { background-color: transparent; border-image: url(" +
            config.newiqamah+") 0 0 0 0 stretch stretch;}")
        self.setVisible(False)
        self.config = config
