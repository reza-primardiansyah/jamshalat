# -*- coding: utf-8 -*-                 # NOQA

import sys
import os
import datetime
import time
import json
import locale
import webbrowser
import feedparser
import gc
import traceback
import logging

from PySide2 import QtWidgets, QtGui, QtCore, QtNetwork
from PySide2.QtGui import QPixmap, QBrush, QColor
from PySide2.QtGui import QPainter, QImage, QFont
from PySide2.QtCore import QUrl
from PySide2.QtCore import Qt
from PySide2.QtNetwork import QNetworkReply
from PySide2.QtNetwork import QNetworkRequest
from subprocess import Popen

from lib.Configuration import Configuration
from lib.slideshow import MainSlideFrame, WelcomeFrame, KajianFrame, HaditsFrame, InfoFrame, SupportFrame, AdzanFrame, IqamahFrame, ShalatFrame
from lib.Repository import Repository
from lib.adzan import PlayFullAdzan, NotifyAdzan
from lib.ClockManager import ClockManager

sys.dont_write_bytecode = True
from praytimes import PrayTimes
from datetime import date

logging.basicConfig(filename='PiClockClassic.log',level=logging.DEBUG)
log = logging.getLogger(__name__)

class MainWin(QtWidgets.QWidget):

    def __init__(self, parent):
        QtWidgets.QWidget.__init__(self)
        self.parent = parent

    def keyPressEvent(self, event):
        if isinstance(event, QtGui.QKeyEvent):
            if event.key() == Qt.Key_F4:
                self.parent.myquit()
            if event.key() == Qt.Key_F2:
                self.parent.lastkeytime = time.time() + 2
            if event.key() == Qt.Key_Space:
                self.parent.nextframe(1)
            if event.key() == Qt.Key_Left:
                self.parent.nextframe(-1)
            if event.key() == Qt.Key_Right:
                self.parent.nextframe(1)

            if event.key() == Qt.Key_A:
                self.parent.adzan()
            if event.key() == Qt.Key_I:
                self.parent.iqamah()
            if event.key() == Qt.Key_S:
                self.parent.shalat()

    def mousePressEvent(self, event):
        if type(event) == QtGui.QMouseEvent:
            self.parent.nextframe(1)


class SlideshowClock:
    ADZAN_TO_IQAMAH_DURATION = 10 * 60
    IQAMAH_TO_SHALAT_DURATION = 15
    SHALAT_DURATION = 10 * 60
    SLIDESHOW_DURATION = 10
    UPDATE_DATA_EVERY = 60
    ADZAN_DURATION = 5 * 60


    def changeFrame(self, frame):
        self.currentFrame.setVisible(False)
        if frame != self.iqamahFrame : 
            frame.updateClock(self.timeformat(datetime.datetime.now()))
        self.currentFrame=frame
        self.currentFrame.setVisible(True)

    def adzan(self):
        self.state="ADZAN"
        self.changeFrame(self.adzanFrame)
        adzanTimes = ["SHUBUH","SYURUQ","ZHUHUR","ASHAR", "MAGHRIB", "ISYA"]
        adzanTime = adzanTimes[self.alarm_index]
        if(adzanTime != "SYURUQ"):
            self.adzanPlayer.play(adzanTime)

    def iqamah(self):
        print("changing to iqamah")
        self.state="IQAMAH"
        self.changeFrame(self.iqamahFrame)
        print("changed to iqamah")

    def shalat(self):
        print("changing to shalat")
        self.state="SHALAT"
        self.changeFrame(self.shalatFrame)
        print("changed to shalat")

    def showNormal(self):
        print("changing to normal")
        self.state="NORMAL"
        self.changeFrame(self.mainSlideFrame)
        print("changed to normal")

    def updateClock(self, now):
        if self.state=="NORMAL":
            print("updating Clock")
            timestr = self.timeformat(now)
            self.currentFrame.updateClock(timestr)
            print("updated Clock")

    def timeformat(self, now):
        timestr = self.config.digitalformat.format(now)
        if self.config.digitalformat.find("%I") > -1:
            if timestr[0] == '0':
                timestr = timestr[1:99]
        return timestr

    def updateData(self):
        now = datetime.datetime.now()
        self.updateKajian()
        self.updateHadits()
        self.updateInfo()
        self.fetchWebConfig()
        log.debug("updateData = " + str(datetime.datetime.now() - now))

    def updateKajian(self):
        print("updating Kajian")
        now = datetime.datetime.now()
        self.kajianList =  self.repository.getKajian()
        print("updated Kajian")
        log.debug("updateKajian = " + str(datetime.datetime.now() - now))

    def updateHadits(self):
        print("updating Hadits")
        now = datetime.datetime.now()
        self.haditsList = self.repository.getHadits()
        print("updated Hadits")
        log.debug("updateHadits = " + str(datetime.datetime.now() - now))

    def updateInfo(self):
        print("updating Info")
        now = datetime.datetime.now()
        self.infoList = self.repository.getInfo()
        print("updated Info")
        log.debug("updateInfo = " + str(datetime.datetime.now() - now))

    def fetchWebConfig(self):
        print("updating Webconfig")
        now = datetime.datetime.now()
        webConfig = repository.getSetting()[0]["fields"]
        self.config.name = webConfig["nama"]
        self.config.address = webConfig["alamat"]
        self.config.phone = webConfig["telepon"]
        if webConfig["suara"]=="false":
            adzanPlayer.mute()
        else:
            adzanPlayer.unmute()
        self.adzanPlayer.setVolume(int(webConfig["volume"]))
        print("updated Webconfig")
        log.debug("fetchWebConfig = " + str(datetime.datetime.now() - now))

    def nextSlide(self):
        if self.state!="NORMAL":
            return

        self.updateData()
        self.updatePrayTime(datetime.datetime.now())
        self.updateNamaMasjid(self.config.name, self.config.address, self.config.phone)
        gc.collect()

        if(self.currentFrame == self.kajianFrame):
            if(len(self.kajianList)>0):
                self.kajianIndex = (self.kajianIndex+1) % len(self.kajianList)
            if(self.kajianIndex > 0):
                print("updating Kajian Display")
                self.kajianFrame.updateKajianDisplay(self.kajianList[self.kajianIndex])
                print("updated Kajian Display")
            else:
                print("leaving Kajian Display")
                self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
                nextframe = self.slideFrames[self.slideIndex]
                self.changeFrame(nextframe)
                print("left Kajian Display")

        elif(self.currentFrame == self.infoFrame):
            if(len(self.infoList)>0):
                self.infoIndex = (self.infoIndex+1) % len(self.infoList)
            if(self.infoIndex > 0):
                print("updating Info Display")
                self.infoFrame.updateInfoDisplay(self.infoList[self.infoIndex])
                print("updated Info Display")
            else:
                print("leaving Info Display")
                self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
                nextframe = self.slideFrames[self.slideIndex]
                self.changeFrame(nextframe)
                print("left Info Display")

        else:
            self.slideIndex = (self.slideIndex+1) % len(self.slideFrames)
            nextframe = self.slideFrames[self.slideIndex]
            if(nextframe == self.kajianFrame):
                self.updateKajian()
                if(len(self.kajianList)==0):
                    self.kajianFrame.showDefaultKajianDisplay()
                else:
                    self.kajianIndex = 0
                    self.kajianFrame.updateKajianDisplay(self.kajianList[self.kajianIndex])
            if(nextframe == self.haditsFrame):
                self.updateHadits()
            if(nextframe == self.infoFrame):
                self.updateInfo()
                if(len(self.infoList)==0):
                    self.infoFrame.showDefaultInfoDisplay()
                else:
                    self.infoIndex = 0
                    self.infoFrame.updateInfoDisplay(self.infoList[self.infoIndex])
            self.changeFrame(nextframe)

        if self.currentFrame == self.haditsFrame:
            if(len(self.haditsList)>0):
                print("updating Hadits Display")
                self.haditsFrame.updateHaditsDisplay(self.haditsList[self.haditsIndex])
                self.haditsIndex = (self.haditsIndex+1) % len(self.haditsList)
                print("updated Hadits Display")

    def tick1(self):
        self.tick()
        self.nextSlide()

    def tick(self):
        now = datetime.datetime.now()
        self.doTick(now)

    def doTick(self, now):
        if self.tickLock == True:
            return

        realnow = datetime.datetime.now()
        log.debug("tick time = " + now.strftime("%H:%M:%S.%f"))
        log.debug("real time = " + realnow.strftime("%H:%M:%S.%f"))
        log.debug("time diff = " + str(realnow - now))
        try:
            self.tickLock = True
            if self.config.DateLocale != "":
                try:
                    locale.setlocale(locale.LC_TIME, 'IND')
                except:
                    pass

            timestr = self.timeformat(now)
            if self.lasttimestr != timestr:
                self.updateClock(now)

            self.handleState()
            self.lasttimestr = timestr

            # Set format tanggal indonesia, hendra    
            if now.day != self.lastday:
                self.lastday = now.day
                self.updateDateDisplay(now)

        except Exception as exc:
            tb = traceback.TracebackException.from_exception(exc)
            print("EXCEPTION doTick")
            print("=========")
            print(''.join(tb.stack.format()))
            print("=========")
            print(traceback.format_exc())
            print("=========")
            log.exception("Exception in doTick")

        finally:
            self.tickLock = False

        endofcall = datetime.datetime.now()
        log.debug("run time  = " + str(endofcall - realnow))



    def updateDateDisplay(self, now):
        # date
        day = "{0:%A}".format(now)
        ds = "{0.day} {0:%B} {0.year}".format(now)
        from convertdate import islamic

        islamicDate = islamic.from_gregorian(now.year, now.month, now.day)
        self.mainSlideFrame.updateDate(day, ds, islamicDate)
        self.welcomeFrame.updateDate(day, ds, islamicDate)
        self.kajianFrame.updateDate(day, ds, islamicDate)
        self.haditsFrame.updateDate(day, ds, islamicDate)
        self.infoFrame.updateDate(day, ds, islamicDate)
        self.supportFrame.updateDate(day, ds, islamicDate)

        self.data_adzan = self.updateDataAdzan()
        self.mainSlideFrame.updateSholatDisplay([i.waktu.strftime("%H:%M") for i in self.data_adzan])

    def updateDataAdzan(self):
        times = self.repository.getJadwalSholat()
        return times.as_list()

    def handleState(self):
        now = datetime.datetime.now()
        self.alarm_index = self.getCurrentTimeIndex(now)
        pray_time = self.data_adzan[self.alarm_index]
        
        if self.state=="NORMAL":
            isTimeForAdzan = self.clockManager.isTimeForAdzan(now, pray_time)
            countdown = self.clockManager.time_iqamah_seconds(now, pray_time)
            log.debug("NORMAL, now="+now.strftime("%H:%M:%S")+", pray_time="+str(pray_time)+", countdown=" +str(countdown)+ ", isTimeForAdzan="+str(isTimeForAdzan))
        if self.state=="NORMAL" and self.clockManager.isTimeForAdzan(now, pray_time):
            self.adzan()
        # print("state1 = %s"%self.state)

        if self.state=="ADZAN" and self.clockManager.isTimeToShowNormalBeforeIqamah(now, pray_time):
            self.updatePrayTime(now)
            self.showNormal()
        # print("state2 = %s"%self.state)

        if self.clockManager.isTimeForIqamah(now, pray_time):
            self.iqamah()
        # print("state3 = %s"%self.state)

        if self.state=="IQAMAH":
            deltastr = str(datetime.timedelta(seconds=self.clockManager.time_iqamah_seconds(now, pray_time)))
            self.iqamahFrame.setCountdown(deltastr)
            if self.clockManager.isTimeForShalat(now, pray_time):
                self.shalat()
        # print("state4 = %s"%self.state)

        if self.state=="SHALAT":
            if self.clockManager.isShalatDone(now, pray_time):
                self.showNormal()

        log.debug("handleState = " + str(datetime.datetime.now() - now))

    def qtstart(self):
        global ctimer
        global slideshowTimer
        global datas
        datas = self.repository.getKajian()
        slideshowTimer = QtCore.QTimer()
        slideshowTimer.timeout.connect(self.tick1)
        slideshowTimer.start(self.SLIDESHOW_DURATION*1000)
        QtCore.QTimer.singleShot(0, self.tick)

    def realquit():
        QtWidgets.QApplication.exit(0)


    def myquit(a=0, b=0):
        global ctimer
        global jadwalTimer
        global slideshowTimer
        ctimer.stop()
        slideshowTimer.stop()
        QtCore.QTimer.singleShot(30, realquit)


    def nextframe(self,plusminus):
        self.frames[self.framep].setVisible(False)
        self.framep += plusminus
        if self.framep >= len(self.frames):
            self.framep = 0
        if self.framep < 0:
            self.framep = len(self.frames) - 1
        self.frames[self.framep].setVisible(True)

    def __init__(self, config, repository, adzanPlayer, clockManager):
        self.repository = repository
        self.clockManager = clockManager
        self.config = config
        self.adzanPlayer = adzanPlayer

        self.lastday = -1
        self.lasttimestr = ""
        self.lastkeytime = 0
        if self.config.DateLocale != "":
            try:
                locale.setlocale(locale.LC_TIME, 'IND')
            except:
                pass

        self.state = 'NORMAL'

        app = QtWidgets.QApplication(sys.argv)
        desktop = app.desktop()
        rec = desktop.screenGeometry()
        height = rec.height()
        width = rec.width()

        w = MainWin(self)

        w.setStyleSheet("QWidget { background-color: black;}")

        xscale = float(width) / 1440.0
        yscale = float(height) / 900.0

        self.frames = []
        self.framep = 0
        self.kajianList = []
        self.haditsList = []
        self.infoList = []
        self.slideFrames = []


        self.initFrames(w, width, height)
        self.updateData()
        self.updateNamaMasjid(self.config.name, self.config.address, self.config.phone)
                
        self.data_adzan = self.updateDataAdzan()

        self.seconds_hms = [3600, 60, 1] # Number of seconds in an Hour, Minute, and Second

        nowX = datetime.datetime.now()

        self.adzanplayer = None

        self.updatePrayTime(nowX)
        self.kajianIndex = 0
        self.haditsIndex = 0
        self.infoIndex = 0

        self.tickLock = False

        manager = QtNetwork.QNetworkAccessManager()
        self.mainWin = w
        self.app = app

    def updatePrayTime(self, now):
        print("updating Pray Time")
        self.alarm_index = self.getCurrentTimeIndex(now)
        self.mainSlideFrame.time_active(self.alarm_index)
        print("updated Pray Time")

    def getCurrentTimeIndex(self, now):
        return self.clockManager.current_time_index(self.data_adzan, now)

    def updateNamaMasjid(self, name, address, phone):
        self.welcomeFrame.updateNamaMasjid(name, address, phone)

    def initFrames(self, mainWin, width, height):
       #Frame utama saat pertama kali tampil
        print("make self.mainSlideFrame")

        self.mainSlideFrame = MainSlideFrame(mainWin, self.config, width, height)
        #self.mainSlideFrame.setVisible(False)
        self.frames.append(self.mainSlideFrame)
        print("made self.mainSlideFrame")

        #frame info masjid
        self.welcomeFrame = WelcomeFrame(mainWin, self.config, width, height)
        self.welcomeFrame.setVisible(False)
        self.frames.append(self.welcomeFrame)

        print("made self.welcomeFrame")

        #frame countdown waktu tarhim
        self.kajianFrame = KajianFrame(mainWin, self.config, width, height)
        self.kajianFrame.setVisible(False)
        self.frames.append(self.kajianFrame)
        print("made self.kajianFrame")

        self.haditsFrame = HaditsFrame(mainWin, self.config, width, height)
        self.haditsFrame.setVisible(False)
        self.frames.append(self.haditsFrame)
        print("made self.haditsFrame")

        #frame info atau pengumuman masjid
        self.infoFrame = InfoFrame(mainWin, self.config, width, height)
        self.infoFrame.setVisible(False)
        self.frames.append(self.infoFrame)
        print("made self.infoFrame")

        print("make self.supportFrame")
        self.supportFrame = SupportFrame(mainWin, self.config, width, height)
        self.supportFrame.setVisible(False)
        self.frames.append(self.supportFrame)
        print("made self.supportFrame")

        #frame saat sedang self.adzan
        print("make self.adzanFrame")
        self.adzanFrame = AdzanFrame(mainWin, self.config, width, height)
        self.adzanFrame.setVisible(False)
        self.frames.append(self.adzanFrame)
        print("made self.adzanFrame")

        self.iqamahFrame = IqamahFrame(mainWin, self.config, width, height)
        self.iqamahFrame.setVisible(False)
        self.frames.append(self.iqamahFrame)
        print("made self.iqamahFrame")

        #frame saat sedang melaksanakan shalat
        print("make self.shalatFrame")
        self.shalatFrame = ShalatFrame(mainWin, self.config, width, height)
        self.shalatFrame.setVisible(False)
        self.frames.append(self.shalatFrame)
        print("made self.shalatFrame")

        self.currentFrame = self.mainSlideFrame

        self.slideFrames = [self.mainSlideFrame, self.welcomeFrame, self.kajianFrame, self.haditsFrame, self.infoFrame, self.supportFrame]
        self.slideIndex = 0

    def run(self):
        log.info("STARTING")
        stimer = QtCore.QTimer()
        stimer.singleShot(10, self.qtstart)

        self.mainWin.show()
        self.mainWin.showFullScreen()

        sys.exit(self.app.exec_())


if __name__ == '__main__':
    SERVER="http://localhost:8000"
    config = Configuration.fetch()
    repository = Repository(SERVER)
    # self.adzanPlayer = PlayFullAdzan(self.config)
    adzanPlayer = NotifyAdzan(config)

    clockManager = ClockManager()

    a=SlideshowClock(config, repository, adzanPlayer, clockManager)
    a.run()
